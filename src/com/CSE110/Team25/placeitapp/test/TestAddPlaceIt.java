package com.CSE110.Team25.placeitapp.test;

import com.CSE110.Team25.placeitapp.MainActivity;
import com.robotium.solo.Solo;

import android.test.ActivityInstrumentationTestCase2;

public class TestAddPlaceIt extends ActivityInstrumentationTestCase2<MainActivity> {

	private Solo solo;
	
	public TestAddPlaceIt() {
		super(MainActivity.class);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void setUp() throws Exception {
		super.setUp();
        solo = new Solo(getInstrumentation(), getActivity());
	}
	
	@Override
	protected void tearDown() throws Exception{
		solo.finishOpenedActivities();
	
	}
	
	public void testAdd(){
		solo.assertCurrentActivity("Check on mainactivity", MainActivity.class);
		
	}

}
